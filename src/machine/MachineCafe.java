package machine;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;

import boisson.Boisson;
import boisson.Cafe;
import boisson.Chocolat;
import boisson.Lait;
import boisson.Sucre;

public class MachineCafe implements Serializable {

	/**
	 * Liste des boissons pr�sente dans la machine
	 */
	private ArrayList<Boisson> liste_boisson;

	/**
	 * Hashmap des stocks d'ind�gr�dient
	 */
	private HashMap<String, Integer> stock_ingredient;

	/**
	 * Maximum de boisson dans la machine
	 */
	private final static int MAX_BOISSON = 5;


	/**
	 * Constructeur d'une machine � caf� sans param�tre, initialisation des
	 * variables
	 */
	public MachineCafe(){
		this.liste_boisson = new ArrayList<Boisson>(MAX_BOISSON);
		Cafe caf = new Cafe(10);
		Lait l = new Lait(10);
		Sucre s = new Sucre(10);
		Chocolat choco = new Chocolat(10);

		// Stock d'ingredients de la machine
		this.stock_ingredient = new HashMap<String, Integer>();
		this.stock_ingredient.put(caf.getNom(), caf.getQuantite());
		this.stock_ingredient.put(l.getNom(), l.getQuantite());
		this.stock_ingredient.put(s.getNom(), s.getQuantite());
		this.stock_ingredient.put(choco.getNom(), choco.getQuantite());

		// Creation des listes d'ingredient pour les boissons
		HashMap<String, Integer> ingredientCafe = new HashMap<>();
		ingredientCafe.put(caf.getNom(), 2);
		ingredientCafe.put(s.getNom(), 2);

		HashMap<String, Integer> ingredientLait = new HashMap<>();
		ingredientLait.put(l.getNom(), 2);
		ingredientLait.put(s.getNom(), 2);

		HashMap<String, Integer> ingredientChocolat = new HashMap<>();
		ingredientChocolat.put(choco.getNom(), 2);
		ingredientChocolat.put(s.getNom(), 2);

		// Creation des boissons
		Boisson cafe = new Boisson("Cafe", 2, ingredientCafe);
		Boisson lait = new Boisson("Lait", 1, ingredientLait);
		Boisson chocolat = new Boisson("Chocolat", 3, ingredientChocolat);

		// Ajout des boissons dans la machine
		this.liste_boisson.add(cafe);
		this.liste_boisson.add(lait);
		this.liste_boisson.add(chocolat);

		// Tri.
		this.liste_boisson = this.sort(this.liste_boisson);

		System.out.println("Cr�ation de la machine � caf�.");
		System.out.println("Stock initiale de chaque ingr�dient : 10");
	}

	/**
	 * Ajoute un boisson dans la liste en v�rifiant si le nom n'exista pas
	 * 
	 * @param b
	 * @return
	 */
	public boolean ajouterBoisson(Boisson b) {
		boolean estPresente = false;
		boolean ajouter = false;
		// On regarde si la limite n'est pas atteinte
		if (this.liste_boisson.size() < MAX_BOISSON) {
			// On regarde si le nom n'est pas deja pris
			for (Boisson boisson : this.liste_boisson) {
				if (this.compareName(boisson.getNom(), b.getNom())) {
					estPresente = true;
					break;
				}
			}
			// On ajoute la boisson si le nom n'est pas deja pris
			if (!estPresente) {
				this.liste_boisson.add(b);

				this.liste_boisson = this.sort(this.liste_boisson);
				ajouter = true;
				System.out.println("Boisson " + b.getNom() + " ajout�e.");
			} else {
				System.out.println("Une boisson de ce nom existe d�j�");
			}
		} else {
			System.out.println("Le nombre maximum de boisson est atteint.");
		}
		return ajouter;
	}

	/**
	 * Permet d'acheter une boisson en ins�rant le montant
	 * 
	 * @param b
	 * @param montant
	 */
	public void acheterBoisson(Boisson b, int montant) {
		if (montant >= 0) {
			for (Boisson boisson : this.liste_boisson) {
				// On regarde si la boisson existe
				if (this.compareName(b.getNom(), boisson.getNom())) {
					int solde = montant - b.getPrix();
					if (solde >= 0) {
						// On regarde si il y a assez d'ingredients dans le stock (voir checkQteOk())
						if (this.checkQteOk(b.getListe())) {
							// On decremente les ingredients du stock si c'est ok
							boolean res = this.decrementeIngredient(b.getListe());
							if (!res) {
								break;
							} else {
								System.out.println(
										"Voici votre boisson : " + b.getNom() + " et votre monnaie : " + solde + "�");
							}
						} else {
							System.out.println("Rendu monnaie : " + montant + "�");
						}
					} else {
						System.out.println("Vous n'avez pas ins�r� assez de montant.");
					}
				}
			}
		} else {
			System.out.println("Montant n�gatif.");
		}

	}

	/**
	 * Permet d'ajouter des ingr�dients au stock
	 * 
	 * @param liste_ajout
	 */
	public void ajouterIngredient(HashMap<String, Integer> liste_ajout) {
		// Parcours de la liste en parametre et ajout dans la machine
		for (Entry<String, Integer> entry : liste_ajout.entrySet()) {
			String cle = entry.getKey();
			int valeur = entry.getValue();

			if (this.stock_ingredient.get(cle) != null) {
				int oldValue = this.stock_ingredient.get(cle);
				int newValue = oldValue + valeur;
				this.stock_ingredient.put(cle, newValue);
			}
		}

		System.out.println("Vous venez d'ajouter des ingr�dients dans la machine");
		this.afficherQte();
	}

	/**
	 * Permet de consulter la liste du stock d'ingr�dient disponible de la machine
	 */
	public void afficherQte() {
		System.out.println("Voici le stock : ");
		for (Entry<String, Integer> entry : this.stock_ingredient.entrySet()) {
			String cle = entry.getKey();
			int valeur = entry.getValue();

			System.out.println(cle + ": " + valeur);
		}
	}

	/**
	 * Supprime la boisson de la liste apr�s recherche avec le nom
	 * 
	 * @param nom_boisson
	 */
	public void supprimerBoisson(String nom_boisson) {
		for (Boisson b : this.liste_boisson) {
			if (this.compareName(b.getNom(), nom_boisson)) {
				this.liste_boisson.remove(b);
				System.out.println("La boisson " + nom_boisson + " est supprim�e de la machine.");
				break;
			}
		}
	}

	/**
	 * Permet de modifier le prix et la liste d'ingr�dient d'une boisson. Le nom ne
	 * change pas.
	 * 
	 * @param b
	 */
	public void modifierBoisson(Boisson b) {
		for (Boisson boisson : this.liste_boisson) {
			if (this.compareName(b.getNom(), boisson.getNom())) {
				boisson.setPrix(b.getPrix());
				boisson.setListe(b.getListe());
				break;
			}
		}
		System.out.println("La boisson " + b.getNom() + " a �t� modifi�e.");
	}

	/**
	 * Getter
	 * 
	 * @return liste boisson
	 */
	public ArrayList<Boisson> getListe_boisson() {
		return liste_boisson;
	}


	/**
	 * Permet de compar� deux nom de boisson
	 * 
	 * @param boisson1
	 * @param boisson2
	 * @return
	 */
	private boolean compareName(String boisson1, String boisson2) {
		return boisson1.toLowerCase().equals(boisson2.toLowerCase());
	}

	/**
	 * V�rifie si il y a suffisament de stock par rapport � la quantit� souhait�e
	 * 
	 * @param liste_Ingredient
	 * @return
	 */
	private boolean checkQteOk(HashMap<String, Integer> liste_Ingredient) {
		boolean res = true;
		for (Entry<String, Integer> entry : liste_Ingredient.entrySet()) {
			String cle = entry.getKey();
			int valeur = entry.getValue();
			int stock = this.stock_ingredient.get(cle);
			if (valeur >= 0) {
				if (valeur > stock) {
					res = false;
					System.out.println("Le stock de " + cle + " est insuffisant.");
					break;
				}
			} else {
				res = false;
				System.out.println("Une valeur est negative.");
			}
		}

		return res;
	}

	private ArrayList<Boisson> sort(ArrayList<Boisson> liste) {
		java.util.Collections.sort(liste, new Comparator<Boisson>() {

			@Override
			public int compare(Boisson o1, Boisson o2) {
				return o1.getNom().compareTo(o2.getNom());
			}
		});

		return liste;
	}

	/**
	 * Permet d'enlever les ingr�dients du stocks
	 * 
	 * @param liste_Ingredient
	 * @return
	 */
	private boolean decrementeIngredient(HashMap<String, Integer> liste_Ingredient) {
		boolean res = true;
		for (Entry<String, Integer> entry : liste_Ingredient.entrySet()) {
			String cle = entry.getKey();
			int valeur = entry.getValue();
			if (valeur >= 0) {
				int newStock = this.stock_ingredient.get(cle) - valeur;
				this.stock_ingredient.put(cle, newStock);
			} else {
				res = false;
				System.out.println("La valeur de " + cle + " est n�gative.");
				break;
			}
		}
		return res;
	}


}
