import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Map.Entry;

import boisson.Boisson;
import boisson.Cafe;
import boisson.Chocolat;
import boisson.Lait;
import boisson.Sucre;
import machine.MachineCafe;

public class Main {

	static String negativeValue = "Valeur n�gative.";
	static String incorrectValue = "La valeur est incorrecte.";

	final static int max_sucre = 5;
	final static int min_sucre = 0;

	public static void main(String[] args){
		try {

			Scanner sc = new Scanner(System.in);

			boolean exit = false;

			MachineCafe mc = initialiserMachine();
			
			//Si les donn�es ne sont pas trouv�, 
			//A COMMENTER pour cr�er une sauvegarde automatique avec des donn�es de bases
			//ET EXECUTER LA COMMANDE N�7
			if(mc == null) {
				System.out.println("Aucune donn�es trouv�e");
				exit = true;
			}
			
			while (!exit) {
				System.out.println("");
				afficherMenu();

				// On test si le choix dans le menu est bien un integer pour eviter les choix de
				// type : zakehfrijghsjre
				while (!sc.hasNextInt()) {
					System.out.println("Entrez une valeur correcte.");
					afficherMenu();
					sc.nextLine();
				}

				int value = sc.nextInt();

				switch (value) {
				case 1:
					if (mc.getListe_boisson().size() == 0) {
						System.out.println("Liste de boissons vide, (2) pour ajouter.");
					} else {
						System.out.println("Achat d'une boisson :");
						acheterBoisson(mc, sc);
					}
					break;
				case 2:
					System.out.println("Ajout d'une nouvelle boisson : ");
					ajouterBoisson(mc, sc);
					break;
				case 3:
					System.out.println("Ajout des ingr�dients :");

					HashMap<String, Integer> ajoutStock = makeHashMapValue(sc);

					mc.ajouterIngredient(ajoutStock);
					break;
				case 4:
					mc.afficherQte();
					break;
				case 5:
					if (mc.getListe_boisson().size() == 0) {
						System.out.println("Liste de boissons vide, (2) pour ajouter.");
					} else {
						System.out.println("Suppression d'une boisson");
						supprimerBoisson(mc, sc);
					}

					break;
				case 6:
					if (mc.getListe_boisson().size() == 0) {
						System.out.println("Liste de boissons vide, (2) pour ajouter.");
					} else {
						System.out.println("Modification d'une boisson :");
						modifierBoisson(mc, sc);
					}

					break;
				case 7:
					mc = new MachineCafe();
					System.out.println("Machine reinitialis�e");
					break;
				case 8:
					exit = true;
					System.out.println("Arr�t de la machine.");
					break;
				default:
					afficherMessageErreur(incorrectValue);
					break;
				}

				enregistrerMachine(mc);

				// Apres chaque op�ration on demande si l'utilisateur veut en faire une
				// nouvelle, sinon on coupe la machine
				boolean again = false;
				while (!again) {
					System.out.println("");
					System.out.println("Nouvelle op�ration ? (O/N)");
					String valueNext = sc.next();
					switch (valueNext.toLowerCase()) {
					case "o":
						again = true;
						break;
					case "n":
						again = true;
						exit = true;
						System.out.println("Arr�t de la machine.");
						break;
					default:
						afficherMessageErreur(incorrectValue);
						break;
					}
				}

			}

			sc.close();
		} catch (NullPointerException n) {
			System.out.println("Donn�es introuvables");
		}
	}

	/**
	 * Permet de modifier une boisson
	 * 
	 * @param mc
	 * @param scanner
	 */
	private static void modifierBoisson(MachineCafe mc, Scanner scanner) {
		boolean estModifie = false;
		boolean qteOk = false;
		boolean isLiquidOk = false;
		boolean prixOk = false;
		do {
			try {
				int j = 0;
				// Affiche des boissons
				for (Boisson boisson : mc.getListe_boisson()) {
					j++;
					System.out.println(j + ":" + boisson);
				}
				System.out.println("Quelle boisson modifier (selectionner le num�ro) ?");

				// Scanner choix de la boisson a modifier
				int choixUpdateBoisson = affectWithNegativeTest(scanner);

				// Recherche la boisson par rapport au choix. On fait -1 car la liste commence a
				// 0 alors qu'a l'affichage la liste commence a 1.
				Boisson boisson = mc.getListe_boisson().get(choixUpdateBoisson - 1);

				System.out.println("Nouveau prix : ");

				// Tant que le prix est 0 on continue de demander un prix
				do {
					int newPrice = affectWithNegativeTest(scanner);
					if (newPrice == 0) {
						afficherMessageErreur("Le prix ne peut pas �tre 0�");
					} else {

						boisson.setPrix(newPrice);
						prixOk = true;
					}
				} while (!prixOk);

				// On demande ici les ingredients. Si toutes les valeures sont � 0 alors on
				// recommence car on attends au moins 1 valeure.
				System.out.println("Ingredient :");
				do {
					for (Entry<String, Integer> entry : boisson.getListe().entrySet()) {
						String cle = entry.getKey();

						System.out.println("Nouvelle quantite pour " + cle + " :");

						int newQte = affectWithNegativeTest(scanner);

						boisson.getListe().put(cle, newQte);

					}
					for (Entry<String, Integer> entry : boisson.getListe().entrySet()) {
						
						if (entry.getValue() > 0) {
							qteOk = true;
							break;
						}
					}
					if (!qteOk) {
						afficherMessageErreur("Il n'y a plus d'ingr�dients.");
					} else {
						isLiquidOk = isLiquide(boisson.getListe());
						if (!isLiquidOk) {
							afficherMessageErreur("Il doit y avoir un liquide dans la boisson.");
						}
					}

				} while (!isLiquidOk);

				mc.modifierBoisson(boisson);
				estModifie = true;
			} catch (IndexOutOfBoundsException e) {
				afficherMessageErreur(incorrectValue);
			} catch (InputMismatchException e2) {
				afficherMessageErreur(incorrectValue);
				scanner.nextLine();
			}
		} while (!estModifie);
	}

	/**
	 * Suppression d'une boisson
	 * 
	 * @param mc
	 * @param scanner
	 */
	private static void supprimerBoisson(MachineCafe mc, Scanner scanner) {
		boolean estSupprime = false;
		do {
			try {
				int k = 0;
				// Affichage des boissons
				for (Boisson boisson : mc.getListe_boisson()) {
					k++;
					System.out.println(k + ":" + boisson);
				}

				System.out.println("Choisissez votre boisson (selectionner le num�ro): ");

				int boissonToSupr = scanner.nextInt();

				// Recherche la boisson par rapport au choix. On fait -1 car la liste commence a
				// 0 alors qu'a l'affichage la liste commence a 1.
				Boisson supprboisson = mc.getListe_boisson().get(boissonToSupr - 1);

				mc.supprimerBoisson(supprboisson.getNom());
				estSupprime = true;

			} catch (IndexOutOfBoundsException e) {
				afficherMessageErreur(incorrectValue);
			} catch (InputMismatchException e2) {
				afficherMessageErreur(incorrectValue);
				scanner.nextLine();
			}
		} while (!estSupprime);

	}

	/**
	 * Ajout d'une boisson dans la machine
	 * 
	 * @param mc
	 * @param scanner
	 */
	private static void ajouterBoisson(MachineCafe mc, Scanner scanner) {
		boolean estAjoute = false;
		boolean prixOk = false;
		do {
			try {
				System.out.println("Nom de la boisson :");
				scanner.nextLine();
				String nom = scanner.nextLine();

				// Ici on affecte le prix, on ne peut pas affecter avec un montant nul
				System.out.println("Prix de la boisson :");
				int prix = affectWithNegativeTest(scanner);
				while (!prixOk) {
					if (prix == 0) {
						afficherMessageErreur("Montant nul");
						prix = affectWithNegativeTest(scanner);
					} else {
						prixOk = true;
					}
				}

				System.out.println("Ingr�dients:");

				HashMap<String, Integer> ingredient = makeHashMapValue(scanner);
				boolean ingredientOk = isLiquide(ingredient);
				// Creation de la liste d'ingr�dients, voir la fonction makeHashMapValue() pour
				// plus de d�tails
				if (!ingredientOk) {
					while (!ingredientOk) {
						System.out.println("Une boisson doit avoir un liquide");
						ingredient = makeHashMapValue(scanner);
						ingredientOk = isLiquide(ingredient);
					}
				}

				// On ajoute automatiquement du sucre a 0 s'il n'y en a pas
				Sucre defaultSucre = new Sucre(0);
				if (!ingredient.containsKey(defaultSucre.getNom())) {
					ingredient.put(defaultSucre.getNom(), defaultSucre.getQuantite());
				}

				Boisson boiss = new Boisson(nom, prix, ingredient);

				mc.ajouterBoisson(boiss);
				estAjoute = true;
			} catch (IndexOutOfBoundsException e) {
				afficherMessageErreur(incorrectValue);
			} catch (InputMismatchException e2) {
				afficherMessageErreur(incorrectValue);
				scanner.nextLine();

			}
		} while (!estAjoute);
	}

	/**
	 * Permet d'acheter une boisson
	 * 
	 * @param mc
	 * @param scanner
	 */
	private static void acheterBoisson(MachineCafe mc, Scanner scanner) {
		boolean estAchete = false;
		boolean montantOk = false;
		do {
			try {
				int i = 0;
				for (Boisson boisson : mc.getListe_boisson()) {
					i++;
					System.out.println(i + ":" + boisson);
				}

				System.out.println("Choisissez votre boisson :");

				int valueBoisson = scanner.nextInt();

				// Recherche la boisson par rapport au choix. On fait -1 car la liste commence a
				// 0 alors qu'a l'affichage la liste commence a 1.
				Boisson b = mc.getListe_boisson().get(valueBoisson - 1);

				// Choix de la quantite de sucre pour la boisson, voir ajouterSucre()
				System.out
						.println("Choisir votre quantit� de sucre (min: " + min_sucre + ", max: " + max_sucre + ") :");
				Boisson newBoisson = ajouterSucre(b, scanner);

				System.out.println("Inserer votre montant (montant entier): ");

				// Paiement, demande une valeur tant que la somme ins�r� est inf�rieure au prix.
				System.out.println("Prix : " + b.getPrix() + "�");
				int montant = affectWithNegativeTest(scanner);
				while (!montantOk) {
					if (montant < b.getPrix()) {
						System.out.println("Montant insuffisant, inserez plus : ");
						System.out.println("Prix : " + b.getPrix() + "�");
						System.out.println("Votre montant : " + montant + "�");
						montant += affectWithNegativeTest(scanner);
					} else {
						System.out.println("Montant ok");
						montantOk = true;
					}
				}

				mc.acheterBoisson(newBoisson, montant);
				estAchete = true;
			} catch (IndexOutOfBoundsException e) {
				afficherMessageErreur(incorrectValue);
			} catch (InputMismatchException e2) {
				afficherMessageErreur(incorrectValue);
				scanner.nextLine();
			}
		} while (!estAchete);
	}

	/**
	 * Ajout du sucre pour la boisson donn�e
	 * 
	 * @param b
	 * @param scanner
	 * @return
	 */
	private static Boisson ajouterSucre(Boisson b, Scanner scanner) {
		boolean fin = false;
		// Pour eviter l'update des ingr�dient de la boisson, on cree une boisson
		// temporelle qui sera vendue.
		HashMap<String, Integer> listeTemp = new HashMap<>();
		listeTemp.putAll(b.getListe());
		Boisson boisson_res = new Boisson(b.getNom(), b.getPrix(), listeTemp);
		Sucre sucre = new Sucre(0);

		do {
			try {
				System.out.println("Sucre :" + boisson_res.getListe().get(sucre.getNom()));
				System.out.println("+, -, ok (pour valider) :");
				String choix = scanner.next();

				// Operation d'incrementation et de decrementation sur la boisson
				switch (choix.toLowerCase()) {
				case "+":
					// On regarde si le max serait atteint si l'op�ration est effectu� alors on
					// bloque sinon on op�re
					if (boisson_res.getListe().get(sucre.getNom()) + 1 > max_sucre) {
						System.out.println("Maximum atteint");
					} else {
						boisson_res.operationIngredient(sucre.getNom(), "+");
					}
					break;
				case "-":
					// On regarde si le min serait atteint si l'op�ration est effectu� alors on
					// bloque sinon on op�re
					if (boisson_res.getListe().get(sucre.getNom()) - 1 < min_sucre) {
						System.out.println("Minimum atteint");
					} else {
						boisson_res.operationIngredient(sucre.getNom(), "-");
					}
					break;
				case "ok":
					fin = true;
					break;
				default:
					afficherMessageErreur(incorrectValue);
					break;
				}
			} catch (InputMismatchException e2) {
				afficherMessageErreur(incorrectValue);
				scanner.nextLine();
			}
		} while (!fin);

		return boisson_res;
	}

	/**
	 * Affichage du menu
	 */
	private static void afficherMenu() {
		System.out.println("Menu : ");
		System.out.println("(1) Acheter une boisson");
		System.out.println("(2) Ajouter une boisson");
		System.out.println("(3) Ajouter des ingr�dients");
		System.out.println("(4) Afficher le stock");
		System.out.println("(5) Supprimer une boisson");
		System.out.println("(6) Modifier une boisson");
		System.out.println("(7) R�initialiser la machine");
		System.out.println("(8) Quitter");
		System.out.println("");
		System.out.println("Que voulez-vous faire ? (Entrez le num�ro)");
	}

	/**
	 * Creation de la liste d'ingredients
	 * 
	 * @param scanner
	 * @return
	 */
	private static HashMap<String, Integer> makeHashMapValue(Scanner scanner) {
		boolean estFait = false;
		HashMap<String, Integer> resHashmap = null;
		do {
			try {
				boolean goodValue = false;

				while (!goodValue) {
					resHashmap = new HashMap<>();

					// On demande la quantite pour chaque type d'ingredient
					System.out.println("Combien de lait ?");
					Lait lait = new Lait(affectWithNegativeTest(scanner));
					resHashmap.put(lait.getNom(), lait.getQuantite());

					System.out.println("Combien de cafe ?");
					Cafe cafe = new Cafe(affectWithNegativeTest(scanner));
					resHashmap.put(cafe.getNom(), cafe.getQuantite());

					System.out.println("Combien de chocolat ?");
					Chocolat chocolat = new Chocolat(affectWithNegativeTest(scanner));
					resHashmap.put(chocolat.getNom(), chocolat.getQuantite());

					System.out.println("Combien de sucre ?");
					Sucre sucre = new Sucre(affectWithNegativeTest(scanner));
					resHashmap.put(sucre.getNom(), sucre.getQuantite());

					// On parcours la liste et on supprime les ingredients avec 0 en quantite
					Iterator it = resHashmap.entrySet().iterator();
					while (it.hasNext()) {
						Entry<String, Integer> ingredient = (Entry<String, Integer>) it.next();

						if (ingredient.getValue() == 0) {
							it.remove();
						}
					}

					// Si la liste est vide apr�s retrait des ingredients alors on recommence
					// l'op�ration car une boisson ne peut etre creee avec aucun ingr�dient.
					if (resHashmap.isEmpty()) {
						afficherMessageErreur("Aucun ingr�dient.");
					} else {
						// On test s'il n'y a pas que du sucre dans la boisson
						goodValue = true;
						estFait = true;
					}
				}
			} catch (IndexOutOfBoundsException e) {
				afficherMessageErreur(incorrectValue);
			} catch (InputMismatchException e2) {
				afficherMessageErreur(incorrectValue);
				scanner.nextLine();
			}

		} while (!estFait);

		return resHashmap;
	}

	/**
	 * Permet de verifier si au moins un liquide est present
	 * 
	 * @param resHashmap
	 * @return
	 */
	private static boolean isLiquide(HashMap<String, Integer> resHashmap) {
		boolean contient = false;
		//Variables pour test 
		Cafe cafe = new Cafe(0);
		Chocolat chocolat = new Chocolat(0);
		Lait lait = new Lait(0);
		if (resHashmap.containsKey(cafe.getNom())) {
			if (resHashmap.get(cafe.getNom()) > 0) {
				contient = true;
			}
		} else if (resHashmap.containsKey(chocolat.getNom())) {
			if (resHashmap.get(chocolat.getNom()) > 0) {
				contient = true;
			}
		} else if (resHashmap.containsKey(lait.getNom())) {
			if (resHashmap.get(lait.getNom()) > 0) {
				contient = true;
			}
		}
		return contient;
	}

	/**
	 * Methode appel� pour l'affectation d'integer on test les valeur entr�es au
	 * scanner si elle sont n�gative pour �viter les erreurs
	 * 
	 * @param sc
	 * @return
	 */
	public static int affectWithNegativeTest(Scanner sc) {
		int res = 0;
		boolean estInteger = false;
		do {
			try {
				int value = sc.nextInt();
				res = value;
				estInteger = true;
			} catch (InputMismatchException e2) {
				afficherMessageErreur(incorrectValue);
				sc.nextLine();
			}
		} while (!estInteger);

		while (res < 0) {
			afficherMessageErreur(negativeValue);
			res = sc.nextInt();
		}

		return res;
	}

	/**
	 * Affiche un message d'erreur uniform
	 * 
	 * @param message
	 */
	public static void afficherMessageErreur(String message) {
		System.out.println(message);
		System.out.println("Entrez une valeure correcte.");
	}

	public static MachineCafe initialiserMachine() {
		MachineCafe mc = null;
		try {
			System.out.println("Initialisation de la machine");
			FileInputStream fileIn = new FileInputStream("machine.txt");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			mc = (MachineCafe) in.readObject();
			enregistrerMachine(mc);
			in.close();
			fileIn.close();
		} catch (IOException i) {
			i.printStackTrace();

		} catch (ClassNotFoundException c) {
			c.printStackTrace();
		}

		return mc;

	}

	public static void enregistrerMachine(MachineCafe mc) {
		try {
			FileOutputStream fileOut = new FileOutputStream("machine.txt");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(mc);
			out.close();
			fileOut.close();
		} catch (IOException i) {
			i.printStackTrace();
		}
	}
}
