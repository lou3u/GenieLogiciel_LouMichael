package boisson;

import java.io.Serializable;

public abstract class Ingredient implements Serializable {

	private String nom;
	private int quantite;
	
	public Ingredient(int qte){
		this.quantite = 10;
	}
	
	public Ingredient(){
		this.quantite = 0;
	}
	
	void ajouterQuantite(int qte){
		this.quantite += qte;
	}
	
	void supprimerQuantite(int qte){
		this.quantite -= qte;
	}
	
	public int getQuantite(){
		return this.quantite;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
}
