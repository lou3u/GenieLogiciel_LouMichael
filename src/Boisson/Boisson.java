package boisson;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class Boisson implements Serializable{
	
	private String nom;
	private int prix;
	private HashMap<String,Integer> liste;

	
	public Boisson(String nom, int prix, HashMap<String,Integer>  liste){
		this.nom = nom;
		this.prix = prix;
		this.liste = liste;
	}
	
	
	public String toString(){
		String res = "";
		String listeIngredient = "";
		for(Entry<String, Integer> entry : this.liste.entrySet()) {
		    String cle = entry.getKey();
		    int valeur = entry.getValue();
			listeIngredient += cle + ": " + valeur+" ";
		
		}
		res += this.nom + ": " + this.prix + "�, Ingredients [" + listeIngredient+']';
		
		return res;
	}
	
	/**
	 * Permet de decrementer on increment des ingredients d'une boisson
	 * @param ingredient
	 * @param operation
	 */
	public void operationIngredient(String ingredient,String operation) {
		int value = this.liste.get(ingredient);
		if(operation.equals("+")) {
			value = value + 1;
			this.liste.put(ingredient, value);
		}else if(operation.equals("-")) {
			value = value - 1;
		 	this.liste.put(ingredient, value);
		}
		    
	}
	
	public void setPrix(int prix) {
		this.prix = prix;
	}
	
	public void setListe(HashMap<String, Integer> liste) {
		this.liste = liste;
	}
	
	public String getNom() {
		return this.nom;
	}
	
	public int getPrix() {
		return this.prix;
	}
	
	public HashMap<String, Integer> getListe() {
		return liste;
	}
}
